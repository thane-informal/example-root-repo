# Automated Submodule Updates

## The Problem
As per [this issue][1], we envisage that we will need to make use of Git
submodules in order to implement **read-level access control** for certain
facets of our storage. For example, a "root"-level repository can be fully
accessible by all users, whereas a submodule may have read restrictions on it
such that only certain users can access that submodule.

Git submodules are notoriously tricky to manage, and having to update them
manually, as well as differing levels of permissions in different repositories,
increases contribution friction significantly.

## The Solution
One way of easing the friction in the use of Git submodules is to use CI to
automatically update submodules.

For example, if one had the following repository configuration:

```
|_ example-root-repo/.git   - Root Git repository (parent node)
   |_ folder1/              - Simple folder in the root
   |_ folder2/.git          - Submodule "example-sub-repo1" (leaf node)
   |_ folder3/.git          - Submodule "example-sub-repo2" (parent and leaf node)
      |_ folder3-1/.git     - Submodule "example-sub-repo3" (leaf node)
```

The minimum CI workflow required to facilitate automatic update of submodules is
as follows.

```mermaid
graph LR
    s1(build):::definite --> s2(push_submodules_update):::optional
    s2 --> s3(update_parent_modules):::optional

    classDef definite stroke-width:4px
    classDef optional stroke-width:4px,stroke-dasharray:5,5
```

* `build` would be your usual application build stage. There could be more
  stages after this, like `test` and/or `deploy`, but prior to
  `push_submodules_update`. To ensure repository integrity, it is recommended
  that your build stage:
  1. first recursively update all submodules, and
  2. then execute the build.
* `push_submodules_update` is optionally triggered on `build` stage success, and
  only when a submodule is to be updated. This stage:
  1. Updates submodules
  2. Commits and pushes the submodule update (requires the appropriate
  permissions)
* `update_parent_modules` is optionally triggered on `build` stage success and
  if we needed to update one or more submodules. This stage [triggers][2] the
  parent repository's pipeline to start and also update its submodules.

### GitLab Implementation

**NOTE**: GitLab CI clones repositories over HTTPS. This is problematic when
one's submodules' URLs are SSH-style (`git://`), and requires some configuration
tricks to allow submodule updates during CI execution.

#### Parent Node Configuration

A **parent node** repository is one that has submodules, but is not a submodule
in any other repositories. Parent nodes need:

1. [A trigger to be present][3], so that submodules, when updated, can trigger
   the parent node's pipeline.
2. If submodules are cloned over SSH:
   1. You need a [CI variable][4] that contains a private SSH key.
   2. You need to either configure your own user account to allow access to the
      public key correlating to the private SSH key, or you need to create a
      "bot" user account on GitLab to which you grant write access to your
      repository.
3. **NB**: When pushing updates from CI you must always remember to do so in a
   way that [skips CI][5], so that you don't end up in an infinite CI loop.

#### Leaf Node Configuration

A **leaf node** repository is one that has no submodules, and is a submodule
within one or more parent repositories. Leaf nodes need:

1. The **trigger token** from the parent node when you created the [trigger][3].
2. The **project-specific trigger URL** from the parent node when you created
   the [trigger][3]. This looks like:
   `https://gitlab.com/api/v4/projects/${PROJECT_ID}/trigger/pipeline`, where
   `${PROJECT_ID}` is the GitLab project ID for the parent node.

#### Dual Parent/Leaf Configurations

Nodes that are both parents and leaves must simply include all of the
configuration for both.

## Sample GitLab CI Config

```yaml
#
# GitLab CI pipeline definition for parent and leaf-level repository
#

# This image just needs to have Git and curl installed.
image: "thanethomson/submodule-demo:v0.1.1"

variables:
  # Leave this variable empty if there's no parent (i.e. root)
  PARENT_PIPELINE_TRIGGER: "https://gitlab.com/api/v4/projects/17571257/trigger/pipeline"
  GIT_SSH_COMMAND: "ssh -o StrictHostKeyChecking=no -i ~/.ssh/id_rsa-gitlabbot"
  # The SSH URL for the repository. We could infer this from the HTTPS URL.
  CI_REPOSITORY_SSH_URL: "git@gitlab.com:thane-informal/example-sub-repo2.git"
  # This branch's name is arbitrary.
  GIT_TEMP_BRANCH: "submodule-update"

stages:
  - build
  # We first need to push any updates to our own submodules
  - push_submodules_update
  # Then we update the parent repo
  - update_parent

# A YAML template for conditionally configuring submodules.
.submodule_setup_template: &submodule_setup
  - if [ ! -f ".gitmodules" ]; then
      echo "No submodules. Skipping submodule sync.";
    else
      if [ -z "${SSH_DEPLOY_KEY}" ]; then echo "Missing SSH_DEPLOY_KEY environment variable"; exit 1; fi;
      mkdir -p ~/.ssh;
      echo "${SSH_DEPLOY_KEY}" > ~/.ssh/id_rsa-gitlabbot;
      chmod 0600 ~/.ssh/id_rsa-gitlabbot;
      git submodule sync --recursive;
      git submodule update --init --recursive;
      if [ -n "${UPDATE_SUBMODULE}" ]; then git submodule update --remote; fi;
    fi

build:
  # It's highly recommended that you update your submodules prior to doing your
  # build. This would ensure better integrity of your repositories all the way
  # up through the hierarchy.
  before_script: *submodule_setup
  stage: build
  script:
    - echo "Fake build succeeded!"

push submodules update:
  stage: push_submodules_update
  before_script: *submodule_setup
  script:
    # We need a temporary branch to which to commit our changes.
    - git checkout -b "${GIT_TEMP_BRANCH}"
    # Replace these details with your or your bot's details.
    - git config --global user.email "thane@informal.systems"
    - git config --global user.name "Thane Thomson"
    # By default, GitLab clones the repository over HTTPS. To be able to push
    # commits back, we need to explicitly set the SSH-based remote for pushing.
    - git remote set-url --push origin $CI_REPOSITORY_SSH_URL
    - if [ -n "${UPDATE_SUBMODULE}" ]; then git add -u && git commit -m 'Automatically update submodules' && git push -o ci.skip origin "${GIT_TEMP_BRANCH}:master"; fi
  rules:
    # We only execute this stage when we get the $UPDATE_SUBMODULE custom
    # variable. In this example we also require that we're committing to
    # `master` (your workflow may be different).
    - if: '$UPDATE_SUBMODULE == "true" && $CI_COMMIT_BRANCH == "master"'
      when: on_success

# We only trigger a submodule update on the parent when the build successfully
# completes.
update parent modules:
  stage: update_parent
  script:
    # Here we post the $PARENT_PIPELINE_TOKEN (defined through the GitLab UI),
    # and we supply a custom variable $UPDATE_SUBMODULE=true so that the
    # push_submodules_update stage gets executed in the parent.
    - curl -X POST -F token=$PARENT_PIPELINE_TOKEN -F ref=master -F "variables[UPDATE_SUBMODULE]=true" $PARENT_PIPELINE_TRIGGER
  rules:
    # An example of how we only trigger a parent pipeline if we:
    # 1. Know the parent pipeline's trigger URL.
    # 2. Are committing to the `master` branch (this depends on your own
    #    workflow).
    - if: '$PARENT_PIPELINE_TRIGGER != "" && $CI_COMMIT_BRANCH == "master"'
      when: on_success

```

[1]: https://gitlab.com/informalsystems/shared/plaintext/plaintext-orgs/-/issues/16
[2]: https://docs.gitlab.com/ee/ci/triggers/README.html
[3]: https://docs.gitlab.com/ee/ci/triggers/README.html#adding-a-new-trigger
[4]: https://docs.gitlab.com/ee/ci/variables/README.html#via-the-ui
[5]: https://docs.gitlab.com/ee/ci/yaml/#skipping-jobs

