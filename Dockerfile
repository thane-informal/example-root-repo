FROM alpine:latest

RUN apk --update add curl git openssh && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*

